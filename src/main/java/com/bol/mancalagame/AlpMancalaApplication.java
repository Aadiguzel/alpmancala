package com.bol.mancalagame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.bol.mancalagame.controller","com.bol.mancalagame.service","com.bol.mancalagame.config"})
public class AlpMancalaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlpMancalaApplication.class, args);
	}

}
