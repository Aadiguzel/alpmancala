package com.bol.mancalagame.controller;

import com.bol.mancalagame.model.dto.request.GameRequestDto;
import com.bol.mancalagame.model.dto.request.PlayRequestDto;
import com.bol.mancalagame.model.dto.response.GameResponseDto;
import com.bol.mancalagame.model.dto.response.PlayResponseDto;
import com.bol.mancalagame.model.request.GameRequest;
import com.bol.mancalagame.model.request.PlayRequest;
import com.bol.mancalagame.model.response.GameResponse;
import com.bol.mancalagame.model.response.PlayResponse;
import com.bol.mancalagame.service.GameService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping({"/mancala"})
public class GameController {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private GameService gameService;

    @ApiOperation(value = "Create new game", nickname = "createGame", notes = "Create new game with pit and stone count by customer request ")
    @PostMapping("/games")
    public ResponseEntity<GameResponse> createGame(@RequestBody @Valid GameRequest request) {
        GameRequestDto requestDto = modelMapper.map(request, GameRequestDto.class);
        GameResponseDto responseDto = gameService.createGame(requestDto);
        GameResponse gameResponse = modelMapper.map(responseDto, GameResponse.class);
        ResponseEntity<GameResponse> response = new ResponseEntity<GameResponse>(gameResponse, HttpStatus.CREATED);
        return response;
    }

    @ApiOperation(value = "Play game", nickname = "playGame", notes = "Play game by pit index ")
    @PutMapping("/plays")
    public ResponseEntity<PlayResponse> playGame(@RequestBody @Valid PlayRequest playRequest) throws Exception {
        PlayRequestDto playRequestDto = modelMapper.map(playRequest,PlayRequestDto.class);
        PlayResponseDto playResponseDto = gameService.playGame(playRequestDto);
        PlayResponse playResponse = modelMapper.map(playResponseDto,PlayResponse.class);
        ResponseEntity<PlayResponse> response = new ResponseEntity<PlayResponse>(playResponse, HttpStatus.OK);
        return response;
    }
}
