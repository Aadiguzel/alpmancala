package com.bol.mancalagame.service.impl;

import com.bol.mancalagame.config.RuleManager;
import com.bol.mancalagame.model.dto.request.GameRequestDto;
import com.bol.mancalagame.model.dto.request.PlayRequestDto;
import com.bol.mancalagame.model.dto.response.GameResponseDto;
import com.bol.mancalagame.model.dto.response.PlayResponseDto;
import com.bol.mancalagame.model.entity.GameEntity;
import com.bol.mancalagame.model.entity.PlayerEntity;
import com.bol.mancalagame.model.enums.GameStatus;
import com.bol.mancalagame.model.enums.PlayerType;
import com.bol.mancalagame.model.exceptions.GameBusinessException;
import com.bol.mancalagame.service.GameService;
import com.bol.mancalagame.util.GameHelper;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {

    private final GameHelper gameHelper = GameHelper.getInstance();

    @Autowired
    private RuleManager ruleManager;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public GameResponseDto createGame(GameRequestDto requestDto)  {
        GameEntity gameEntity = GameEntity.builder()
                .firstPlayer(createPlayer(PlayerType.FIRST, requestDto.getPitCount()))
                .secondPlayer(createPlayer(PlayerType.SECOND, requestDto.getPitCount()))
                .pits(gameHelper.fillPitsWithStone(requestDto.getPitCount(), requestDto.getStoneCount()))
                .playerTurn(PlayerType.FIRST)
                .status(GameStatus.STARTED)
                .build();
        GameResponseDto gameResponseDto =modelMapper.map(gameEntity, GameResponseDto.class);
        return gameResponseDto;
    }

    @Override
    public PlayResponseDto playGame(PlayRequestDto playRequestDto) throws GameBusinessException {
        PlayResponseDto playResponseDto = new PlayResponseDto();
        gameHelper.validateIndex(playRequestDto);
        gameHelper.iterateStones(playRequestDto,playResponseDto);
        runGameRules(playRequestDto, playResponseDto);
        finishGame(playRequestDto,playResponseDto);
        return playResponseDto;
    }

    public void runGameRules(PlayRequestDto playRequestDto, PlayResponseDto playResponseDto){
        Facts facts = new Facts();
        facts.put("playRequest", playRequestDto);
        facts.put("playResponse", playResponseDto);
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(ruleManager.rules(), facts);
    }

    public void finishGame(PlayRequestDto playRequestDto, PlayResponseDto playResponseDto){
        if(gameHelper.isThereEmptyPit(playResponseDto)){
            PlayerEntity firstPlayer = playRequestDto.getFirstPlayer();
            PlayerEntity secondPlayer = playRequestDto.getSecondPlayer();
            int firstPlayerTotalStones = playResponseDto.getPits()[firstPlayer.getLargePit()] + gameHelper.collectStones(PlayerType.FIRST,playResponseDto.getPits());
            int secondPlayerTotalStones = playResponseDto.getPits()[secondPlayer.getLargePit()] + gameHelper.collectStones(PlayerType.SECOND,playResponseDto.getPits());
            if(firstPlayerTotalStones > secondPlayerTotalStones){
                playResponseDto.setWinnerPlayer(firstPlayer);
            }
            else if(firstPlayerTotalStones < secondPlayerTotalStones){
                playResponseDto.setWinnerPlayer(secondPlayer);
            }
            playResponseDto.setStatus(GameStatus.FINISHED);
        }else{
            playResponseDto.setStatus(GameStatus.PROCESSING);
        }
    }

    public PlayerEntity createPlayer(PlayerType playerType, int pitCount) {
        return PlayerEntity.builder()
                .playerType(playerType)
                .largePit(gameHelper.findLargePitByPlayer(playerType, pitCount))
                .build();
    }

}
