package com.bol.mancalagame.service;

import com.bol.mancalagame.model.dto.request.GameRequestDto;
import com.bol.mancalagame.model.dto.request.PlayRequestDto;
import com.bol.mancalagame.model.dto.response.GameResponseDto;
import com.bol.mancalagame.model.dto.response.PlayResponseDto;

public interface GameService {
    GameResponseDto createGame(GameRequestDto requestDto);

    PlayResponseDto playGame(PlayRequestDto playRequestDto);
}
