package com.bol.mancalagame.model.enums;

public enum GameStatus {
    STARTED,
    PROCESSING,
    FINISHED
}
