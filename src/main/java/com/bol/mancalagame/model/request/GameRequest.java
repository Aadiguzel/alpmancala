package com.bol.mancalagame.model.request;

import com.bol.mancalagame.controller.validator.annotation.GameValidator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@GameValidator
public class GameRequest {
    private int pitCount;
    private int stoneCount;
}