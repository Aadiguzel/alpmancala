package com.bol.mancalagame.model.request;

import com.bol.mancalagame.controller.validator.annotation.PlayValidator;
import com.bol.mancalagame.model.entity.PlayerEntity;
import com.bol.mancalagame.model.enums.GameStatus;
import com.bol.mancalagame.model.enums.PlayerType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@PlayValidator
public class PlayRequest {
    private PlayerEntity firstPlayer;
    private PlayerEntity secondPlayer;
    private PlayerType playerTurn;
    private GameStatus status;
    private int index;
    private int[] pits;
}