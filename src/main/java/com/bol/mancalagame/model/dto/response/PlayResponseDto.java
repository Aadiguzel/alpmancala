package com.bol.mancalagame.model.dto.response;

import com.bol.mancalagame.model.entity.PlayerEntity;
import com.bol.mancalagame.model.enums.GameStatus;
import com.bol.mancalagame.model.enums.PlayerType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayResponseDto {
    private PlayerType playerTurn;
    private int[] pits;
    private GameStatus status;
    private PlayerEntity winnerPlayer;
    private int lastIndex;
}
