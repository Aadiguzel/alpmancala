package com.bol.mancalagame.model.entity;


import com.bol.mancalagame.model.enums.PlayerType;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlayerEntity {
    private PlayerType playerType;
    private int largePit;
}